/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "KO",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "OK",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.7692307692307693, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)  ", "F (Frustration threshold)", "Label"], "items": [{"data": [0.0, 500, 1500, "33 /index.php"], "isController": false}, {"data": [1.0, 500, 1500, "34 /reports"], "isController": false}, {"data": [1.0, 500, 1500, "42 /index.php"], "isController": false}, {"data": [1.0, 500, 1500, "Open risk management menu"], "isController": true}, {"data": [1.0, 500, 1500, "36 /management/index.php"], "isController": false}, {"data": [0.0, 500, 1500, "Submit Risk"], "isController": true}, {"data": [1.0, 500, 1500, "22 /index.php"], "isController": false}, {"data": [1.0, 500, 1500, "41 /logout.php"], "isController": false}, {"data": [1.0, 500, 1500, "Open login page"], "isController": true}, {"data": [0.0, 500, 1500, "Login"], "isController": true}, {"data": [1.0, 500, 1500, "Submit risk with file attachment"], "isController": false}, {"data": [1.0, 500, 1500, "Log out"], "isController": true}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 140, 0, 0.0, 2417.042857142857, 12, 16989, 16612.100000000002, 16766.0, 16989.0, 0.7811802517632354, 2.993648812745514, 1.1774701163679582], "isController": false}, "titles": ["Label", "#Samples", "KO", "Error %", "Average", "Min", "Max", "90th pct", "95th pct", "99th pct", "Throughput", "Received", "Sent"], "items": [{"data": ["33 /index.php", 20, 0, 0.0, 16688.0, 16385, 16989, 16986.6, 16989.0, 16989.0, 0.11244546394998427, 0.5991674116178654, 0.1575664025603832], "isController": false}, {"data": ["34 /reports", 20, 0, 0.0, 36.2, 31, 46, 45.10000000000002, 46.0, 46.0, 0.12405331811612633, 0.42454340625600884, 0.10067217515087984], "isController": false}, {"data": ["42 /index.php", 20, 0, 0.0, 13.9, 13, 17, 16.800000000000004, 17.0, 17.0, 0.12411875682653162, 0.16533006280409096, 0.049574776896534604], "isController": false}, {"data": ["Open risk management menu", 20, 0, 0.0, 37.699999999999996, 35, 50, 48.70000000000003, 50.0, 50.0, 0.12405408758218582, 0.8332461078030021, 0.05172958534921226], "isController": true}, {"data": ["36 /management/index.php", 20, 0, 0.0, 37.699999999999996, 35, 50, 48.70000000000003, 50.0, 50.0, 0.12405485705779096, 0.833251276214342, 0.05172990621452807], "isController": false}, {"data": ["Submit Risk", 20, 0, 0.0, 16919.300000000003, 16587, 17238, 17232.8, 17238.0, 17238.0, 0.11201030494805522, 3.0047311227632942, 1.1818290407717509], "isController": true}, {"data": ["22 /index.php", 20, 0, 0.0, 47.0, 12, 334, 302.60000000000065, 334.0, 334.0, 0.12401255007006708, 0.16603633412701366, 0.05234201576199511], "isController": false}, {"data": ["41 /logout.php", 20, 0, 0.0, 29.099999999999998, 25, 37, 36.800000000000004, 37.0, 37.0, 0.124101043069267, 0.23438614970308827, 0.10155925204301343], "isController": false}, {"data": ["Open login page", 20, 0, 0.0, 47.0, 12, 334, 302.60000000000065, 334.0, 334.0, 0.12389884897969297, 0.16588410346792878, 0.052294025907249325], "isController": true}, {"data": ["Login", 20, 0, 0.0, 16724.200000000004, 16422, 17024, 17021.2, 17024.0, 17024.0, 0.11242018167101359, 0.9837644178882994, 0.24876258754721647], "isController": true}, {"data": ["Submit risk with file attachment", 40, 0, 0.0, 67.44999999999999, 52, 103, 99.49999999999994, 103.0, 103.0, 0.2477700693756194, 1.6844251347249752, 1.5563541408572845], "isController": false}, {"data": ["Log out", 20, 0, 0.0, 43.0, 39, 50, 49.800000000000004, 50.0, 50.0, 0.1240902632574935, 0.399657898655482, 0.1511138264473578], "isController": true}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Percentile 1
            case 8:
            // Percentile 2
            case 9:
            // Percentile 3
            case 10:
            // Throughput
            case 11:
            // Kbytes/s
            case 12:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 140, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
